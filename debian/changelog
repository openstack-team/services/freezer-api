freezer-api (15.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 10:26:58 +0200

freezer-api (15.0.0~rc1-2) experimental; urgency=medium

  * Add python3-migrate as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 14 Sep 2023 11:47:46 +0200

freezer-api (15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 14 Sep 2023 10:34:39 +0200

freezer-api (14.0.0-3) unstable; urgency=medium

  * Cleans properly (Closes: #1044279).

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 16:44:49 +0200

freezer-api (14.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 14:07:50 +0200

freezer-api (14.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed lsb-base depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 15:17:39 +0100

freezer-api (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Mar 2023 16:18:38 +0100

freezer-api (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 22:26:25 +0200

freezer-api (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 18:23:39 +0200

freezer-api (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Sep 2022 09:53:24 +0200

freezer-api (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 21:19:52 +0200

freezer-api (12.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Mar 2022 10:16:33 +0100

freezer-api (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 18:00:48 +0100

freezer-api (11.0.0-2) unstable; urgency=medium

  * Add python3-os-api-ref depends in freezer-api-doc (Closes: #988930).

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Dec 2021 18:17:47 +0100

freezer-api (11.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:04:52 +0200

freezer-api (11.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Blacklist TestSqliteMigrations.test_walk_versions().
  * Add add-header = Connection: close to the uwsgi config.
  * Switch to yaml policy file.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 21:36:06 +0200

freezer-api (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 10:43:55 +0200

freezer-api (10.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 12:44:00 +0200

freezer-api (10.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 19:10:57 +0200

freezer-api (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Fixed (build-)depends for this release.
  * debhelper-compat 11.
  * d/rules: removed --with systemd.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 14:54:44 +0100

freezer-api (9.0.0-2) unstable; urgency=medium

  * Add dh_python3 --shebang=/usr/bin/python3 (Closes: #976722)

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Dec 2020 23:43:52 +0100

freezer-api (9.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Oct 2020 13:03:26 +0200

freezer-api (8.0.0-2) unstable; urgency=medium

  * Edit postrm as freezer-scheduler
  * Add temporary-workaroud-json-validation.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 10 Aug 2020 13:18:55 +0200

freezer-api (8.0.0-1) unstable; urgency=medium

  * New upstream version
  * d/control: Fix requirements
  * d/patches: Remove fix-src-file-to-path-to-backup.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 29 Jul 2020 15:06:28 +0200

freezer-api (7.2.0-3) unstable; urgency=medium

  * d/patches: Add fix-src-file-to-path-to-backup.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 26 Mar 2020 11:50:12 +0100

freezer-api (7.2.0-2) unstable; urgency=medium

  * Rebuilt source-only.

 -- Thomas Goirand <zigo@debian.org>  Thu, 09 Jan 2020 13:40:09 +0100

freezer-api (7.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #945221).

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 21 Nov 2019 13:46:03 +0100
